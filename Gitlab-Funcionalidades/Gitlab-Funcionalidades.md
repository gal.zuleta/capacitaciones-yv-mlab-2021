# GITLAB
- [Cómo acceder](#Acceder) <br>
- [Creación de repositorios](#Repositorios) <br>
- [Creación de grupos](#Crear-Grupos) <br>
- [Creación de subgrupos](#Crear-Subgrupos) <br>
- [Cracióon de Issues](#Creación-Issues) <br>
- [Creación de labels](#Creación-Labels) <br>
- [Manejo y creación de boards](#) <br>
- [Creación de commit y ramas](#Creación-de-commit-y-ramas)<br>
- [Permisos](#Permisos) <br>
- [Roles que cumplen](#Roles-que-cumplen) <br>
- [Agregar Miembros](#Agregar-Miembros) <br><br>

# INTRODUCCIÓN DEL GITLAB
Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.<br><br>
Es un cliente git, que se encarga de almacenar todos nuestros repositorios GIT, prácticamente funciona como un servidor.<br><br>

# Acceder 

- [Ir al inicio](#GITLAB) <br><br>


Primero para entrar al Gitlab debemos buscarlo mediante el navegador. <br><br>
![foto](/imagenes/aa.png) <br><br>

Si en algún caso usted ya tiene seccióon iniciada se redirecionara automaticamente.<br><br>
![foto](/imagenes/a.png) <br><br>

Si no tiene sección iniciada tendrá que aceptar términos y registrarse con alguna cuenta.<br><br>
![foto](/imagenes/bb.png) <br><br>


# Crear Repositorios
- [Ir al inicio](#GITLAB) <br><br>

Se puede crear repositorios una vez registrado en el Gitlab.
En la pantalla de acceso nos sale varios proyectos de creación pero, eligiremos el de crear un repositorio en blanco.<br><br>

![foto](/imagenes/b.png) <br><br>

Al momento de dar clic nosdirecciona a otra página donde llenaremos nuestro proyecto, como queremos que se llame, si lo queremos público, entre otras cosas. <br><br>
![foto](/imagenes/c.png) <br><br>

Al dar clic en crear proyecto nos abrira directamente a nuestra cuenta con el repositorio ya creado.<br><br>
![foto](/imagenes/d.png) <br><br>

Para descargarse el archivo se lo puede hacer varias formas  una de las primeras en en formato comprimido Zip tar.gz entre otros. <br><br>
![foto](/imagenes/e.png) <br><br>

La segunda forma de descargar el archivo es clonando directamente de la consola  a una carpeta determinada.<br><br>
![foto](/imagenes/f.png) <br><br>

# Crear Grupos
- [Ir al inicio](#GITLAB) <br><br>

Creamos grupos dentro de nuestro Gitlab para compartir archivos y comunicarnos entre si.<br>
Para crear nos vamos a diriguir en la parte superior y vamos a dar clic en grupos.<br><br>
![foto](/imagenes/h.png) <br><br>

Después nos va salir un acceso donde llenaremos los datos de nuestro grupo, cuantas personas registradas, el cargo, entre otras cosas.<br><br>

![foto](/imagenes/i.png) <br><br>

Vamos a dar clic en crear grupos y en nuestra cuenta aparece la página con nuestrosproyectos en grupo, si los tendríamos.<br><br>

![foto](/imagenes/j.png) <br><br>

# Crear Subgrupos
- [Ir al inicio](#GITLAB) <br><br>

Creamos los subgrupos para dividir tareas o para estar todos divididos dentro de un gran grupo.<br>
Para crear un sub grupo debemos dirifirnos a la parte superior derecha de nuestra pantalla y damos clic en Nuevo Subgrupo.<br><br>
![foto](/imagenes/k.png) <br><br>

Y selecionaremos como se va a llamar al grupo y en donde quiere que el subgrupo este ubicado, si lo quiere público o provado, sus integrantes entre otras cosas.<br>

![foto](/imagenes/l.png) <br><br>

Al momento de crear nuestro subgropo se posicionara en el grupo seleccionado.
![foto](/imagenes/m.png) <br><br>

# Creación de Issues

Creamos lo issues para la completacionde nuestra tareas.
para crear nuestras carpetas para los deberes, nos debemos de ir a Issues dentro de nuestro repositorio.<br><br>
![foto](/imagenes/n.png) <br><br>

Al dar clic en listas, nos da paso a la pantalla de crear una Nueva Edición.<br><br>
![foto](/imagenes/o.png) <br><br>

Nos va a mandar a la pantalla de registrar nuestra edicion, o nuestro deber, nos da la posibilidad de poner un título, una descrpción, entre otras cosas.<br><br>
![foto](/imagenes/p.png) <br><br>

Al momento de enviar problema nos genera una tarea la cual nos da la posibilidad de aumentar subtareas o el archivo del deber.<br><br>
![foto](/imagenes/q.png) <br><br>

En la parte lateral de nuestra página tenemos unos editores de nuestro issues el cual nos permite poner el peso del deber, etiquetas labels, etc.<br><br>
![foto](/imagenes/r.png) <br><br>

Vamos a crear unas sub tareas para ir marcando al momneto que cada una se haya completado.<br><br>
![foto](/imagenes/s.png) <br><br>

![foto](/imagenes/t.png) <br><br>

![foto](/imagenes/u.png) <br><br>

Y al final para ver nuestros issues debemos dar clic en lista y nos aparecera todas las tareas que hemos creado.<br><br>
![foto](/imagenes/v.png) <br><br>

# Creación Labels
- [Ir al inicio](#GITLAB) <br><br>

El procedimiento es:
- Acceder al repositorio.
- Acceder a Repositorio -> Tags o Etiquetas. 
- Crear una nueva Etiqueta con los datos completos que nos interesen.

# Manejo y creación de boards
- [Ir al inicio](#GITLAB) <br><br>

Haga clic en el menú desplegable con el nombre del tablero actual en la esquina superior izquierda de la página Tableros de temas. <br>
Haz clic en Crear tablero nuevo .<br>
Ingrese el nombre de la nueva placa y seleccione su alcance: hito, etiquetas, cesionario o peso.<br>


# Creación de commit y ramas
- [Ir al inicio](#GITLAB) <br><br>


# Permisos
- [Ir al inicio](#GITLAB) <br><br>

Existen Muchos permisos dentro de nuestro Gitlab, en esta ocación vamos a mostrar los permisos para los grupos. Ejemplo como Invitado, solo podemos visualizar, no vamos a poder editar ni nada.<br>
Como Repositorio Tiene muchos permisos pero no como el de crear carpetas o proyectos.<br>
Como Desarrollador tiene mas permisos aun pero no tiene acceso a compartir carpetas ni proyectos.<br>
Como Mantenedor tiene mas permisos aun como el de compartir proyectos y casi todos, pero no tiene opcion a invitar a grupos.<br>
Como Dueño tiene todos los permisos. <br><br>

![foto](/imagenes/z.png) <br><br>
![foto](/imagenes/aaa.png) <br><br>
![foto](/imagenes/bbb.png) <br><br>


# Roles que cumplen
- [Ir al inicio](#GITLAB) <br><br>

Mantenedor;	Acepta solicitudes de fusión en varios proyectos de GitLab.
Agregado a la página del equipo . Un experto en revisiones de código y conoce el producto / código base.

Crítico;	Realiza revisiones de código en MR.
Agregado a la página del equipo.

Desarrollador;	Tiene acceso a la infraestructura y problemas internos de GitLab (por ejemplo, relacionados con recursos humanos).
Empleado de GitLab o miembro del equipo central (con un NDA).

Contribuyente; Puede hacer contribuciones a todos los proyectos públicos de GitLab.
Tener una cuenta de GitLab.com


# Agregar Miembros
- [Ir al inicio](#GITLAB) <br><br>

Nos vamos a ir a Miembros y automaticamente nos va a parecer la tantalla para agregar a nuestro miembro.<br><br>
![foto](/imagenes/w.png) <br><br>

Llenamos los datos necesarios de nuestro miembro para comparitr nuestro proyecto.<br><br>
![foto](/imagenes/y.png) <br><br>

Después se nos unira el miembro que elijimos. <br><br>
![foto](/imagenes/x.png) <br><br>
