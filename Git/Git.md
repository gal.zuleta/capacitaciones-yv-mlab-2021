# Menú
- [Que es git](#Que-es-git)
- [Comandos de git en consola](#Comandos-de-git-en-consola)

- [Clientes git](#Clientes-git)

- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)

- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)

- [Ramas de kraken](#Ramas-de-kraken)

- [Merge](#Merge)

# Que es git

- [Ir al inicio](#Menú) <br><br>

ES un software de control de versiones diseñado por Linus Torvals, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplocacionnes cuando estas tienen un gran número de archivos de códigos fuente.<br><br>

# Comandos de git en consola
- [Ir al inicio](#Menú) <br><br>

git init, es  un comando que se utiliza una sola vez durante la configuración inicial de un repositorio nuevo.<br><br>
![foto](/imagenes/cc.png) <br><br>

git push, se usa para cargar contenido de repositorio remoto.<br><br>

![foto](/imagenes/dd.png) <br><br>

git add,esto permite construir series de instantaneas de los archivos creados o modificados.<br><br>

![foto](/imagenes/ee.png) <br><br>

git status, muestra el estado del directorios de trabajo y del área del entornor de ensayo.<br><br>

![foto](/imagenes/ff.png) <br><br>


git commit -m, para crear ramas y guardar las modificaciones.<br><br>

![foto](/imagenes/hh.png) <br><br>


# Clientes git
- [Ir al inicio](#Menú) <br><br>

Un cliente Git se usa mayormente para gestionar cáodigo fuente. Se diseño para el mantenimiento de las versiones de las aplicaciones cuando tienen un cáodigo fuente que contiene muchos archivos. <br><br>

![foto](/imagenes/ax.png) <br><br>


# Clonación de proyecto por consola y por cliente

- [Ir al inicio](#Menú) <br><br>

## Clonación por consola.
Copiaremos la URL de nuestra cuenta Gitlab.<br><br>
![foto](/imagenes/f.png) <br><br>

Nos dirigiremos a la carpeta y abrimos Gitbash y ponemos el git clone y la URL.<br><br>

![foto](/imagenes/av.png) <br><br>

Una vex copiado le daremos clic y se nos clonara en la carpeta.<br><br>
![foto](/imagenes/ab.png) <br><br>

Para confirmar abriremos la carpeta y la vamos a visualizar.
![foto](/imagenes/an.png) <br><br>


## Clonación por cliente.


Para clonar el reposito por cliente nos debemos de ir al GitKraken y en file vamos a dar clic en Clone Repo. <br><br>

![foto](/imagenes/az.png) <br><br>

Al momento de dar clic en Clone Repositorio nos va a salir una ventada donde nos indicara de donde queremos clonar.<br><br>

![foto](/imagenes/ac.png) <br><br>

Copiaremos URL que nos da nuestra cuenta de Gitlab.<br><br>

![foto](/imagenes/f.png) <br><br>

Aqui seleccionaremos en donde queremos que se guarde la clonación. <br><br>

![foto](/imagenes/am.png) <br><br>

Finalmente crearemos la clonación dentro de nuestro cliente. <br><br>

![foto](/imagenes/as.png) <br><br>



# Commits por consola y por cliente kraken o smart

- [Ir al inicio](#Menú) <br><br>


## Commits por Consola 

Sirven para crear ramas y guardar las modificaciones.  <br><br>

![foto](/imagenes/hh.png) <br><br>


## Commits por Consola 

Sirven para crear ramas y guardar las modificaciones.  <br><br>

![foto](/imagenes/ad.png) <br><br>

Y creamos nuestra rama 1.  <br><br>

![foto](/imagenes/af.png) <br><br>


# Ramas de kraken
- [Ir al inicio](#Menú) <br><br>

Para crear la mara en nuestro jrajen tenemos que irnos a branch.  <br><br>
![foto](/imagenes/ag.png) <br><br>

Después de dar clic en branch nos sale en donde crearemos nuestra rama poniendo el nombre que queramos.  <br><br>
![foto](/imagenes/aj.png) <br><br>

Finalmente creamos la rama pero arriba de master cosa que no debe estar asi para poner en su lugr usaremos el Mergue para darle sentido.  <br><br>
![foto](/imagenes/ah.png) <br><br>

# Mergue 
- [Ir al inicio](#Menú) <br><br>

El mergue nos sirve para ordenas nuestras ramas y como queremos que nuestras ramas se concten, lo que haremos es mover la rama 1 en master para que se nos conecte.  <br><br>

![foto](/imagenes/al.png) <br><br>

Después de unirlar nos sale unos mensajes de configuracion y selecionaremos la into Rama 1. y finalizamos nuestra configuración.  <br><br>
![foto](/imagenes/ah.png) <br><br>



